import numpy as np
import os
import utils
from gensim.models import Word2Vec
from tqdm import tqdm
import pickle
import json


mappings = '../../matrices/unsupervised/20000/50/'
tdsms_dir = '../../dsms/50/'
gdsm = Word2Vec.load(os.path.join('../../dsms/', 'gdsms.model'))
'''
with open('../../tensorsfp_5000', 'w+') as tensors:
    with open('../../metadatafp_5000', 'w+') as metadata:
        for topic in tqdm(range(50)):

            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
            max_sim_subset_threshold = max(len(tdsm.wv.vocab), 5000)
            gdsm_emb, tdsm_emb, count, words = utils.load_norm(gdsm, tdsm, max_sim_subset_threshold)
            W = np.load(os.path.join(mappings, str(topic) + '.npy'))

            for emb, word in zip(tdsm_emb, words):
                w_i_embeddings = emb.dot(W)
                metadata.write(str(topic) + '_' + str(word) + '\n')
                vector_row = "\t".join([str(x) for x in w_i_embeddings])
                tensors.write(vector_row + '\n')

'''
for topic in tqdm(range(50)):
    mapped = {}
    tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
    max_sim_subset_threshold = max(len(tdsm.wv.vocab), 20000)
    gdsm_emb, tdsm_emb, count, words = utils.load_norm(gdsm, tdsm, max_sim_subset_threshold)
    W = np.load(os.path.join(mappings, str(topic) + '.npy'))

    for emb, word in zip(tdsm_emb, words):
        w_i_embedding = emb.dot(W)
        mapped[(word, topic)] = w_i_embedding

    with open('mapped_space_' + str(topic) + '.pkl', 'wb') as f:
        pickle.dump(mapped, f)

'''
from sklearn.mixture import GaussianMixture

mapped = pickle.load(open('mapped_space_full.pkl', 'rb'))
words = ['python', 'team', 'canada', 'apple', 'toronto', 'act', 'nursery', 'drug']
variance = []

for word in words:
    embs = []

    for i in range(50):
        if (word, i) in mapped:
            embs.append(mapped[(word,i)])

    gmm = GaussianMixture(n_components=1, covariance_type='diag')
    gmm.fit(embs)

    for gauss_cov in gmm.covariances_:
        variance.append([word, np.sum(abs(gauss_cov))])

a = 2
'''









