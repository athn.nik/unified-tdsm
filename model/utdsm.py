#!/usr/bin/env python
from gensim.models import Word2Vec
import numpy as np
import argparse
import logging
import os
import utils
from tqdm import tqdm
from sklearn import preprocessing

def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Unified model of multiple topic-based embeddings')
    # Both corpora are expected to be preprocessed
    parser.add_argument('--corpus_doc', help='the corpus file in document format',
                        default='../../../fenchri/corpora/enwiki/enwiki_doc.cln')
    parser.add_argument('--corpus_sent', help='the corpus file in sentence format',
                        default='../../../fenchri/corpora/enwiki/enwiki_sents.cln')
    parser.add_argument('--size', help='the vector size for the word2vec models', type=int, default=300)
    parser.add_argument('--window', help='the window size for the word2vec models', type=int, default=5)
    parser.add_argument('--cbow', help='choose word2vec model (use 1 for skip-gram model, 0 for cbow model)',
                        choices=[0,1], type=int, default=0)
    parser.add_argument('--topics', help='the number of topics', type=int, default=50)
    parser.add_argument('--anchors', help='number of anchors', type=int, default=5000)
    parser.add_argument('--initial_', help='cardinality of initial set of monosemous anchors', type=int, default=100)
    parser.add_argument('--supervised', help='flag for self learning implementation',
                        choices = ['unsupervised', 'supervised', 'semi_supervised', 'cross_topic'],
                        type=str, default='semi_supervised')
    parser.add_argument('--output_dir', help='flag for self learning implementation', type=str, default='')
    parser.add_argument('--train', help='flag for self training word2vec ', type=bool, default=False)

    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    args = parser.parse_args()
    print("Runnning the " + args.supervised + " version of the code with " + str(args.anchors) + " anchor points....")
    if args.verbose:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    if args.train:
        # Create output directories
        dsms_dir = os.path.join(os.getcwd(),'../../dsms/')
        if not os.path.exists(dsms_dir):
            os.makedirs(dsms_dir)

        tdsms_dir = os.path.join('../../dsms/', str(args.topics))
        if not os.path.exists(tdsms_dir):
            os.makedirs(tdsms_dir)
        lda_dir = '../lda/'
        if not os.path.exists(lda_dir):
            os.makedirs(lda_dir)
        crp_dir = os.path.join('../topic-corpora/', str(args.topics))
        if not os.path.exists(crp_dir):
            os.makedirs(crp_dir)
    else: # not training case only aligning
        dsms_dir = os.path.join(os.getcwd(), '../../dsms/')
        tdsms_dir = os.path.join('../../dsms/', str(args.topics))

        if args.supervised == 'supervised':
            matrices = os.path.join(args.output_dir + '../../matrices/supervised/', str(args.topics))
        elif args.supervised == 'semi_supervised':
            matrices = os.path.join(args.output_dir + '../../matrices/semi_supervised/', str(args.topics))
        elif args.supervised == "unsupervised":
            matrices = os.path.join(args.output_dir + 'matrices/unsupervised/', str(args.topics))
        elif args.supervised == "cross_topic":
            matrices = os.path.join(args.output_dir + '../../matrices/cross_domain/', str(args.topics))

        if not os.path.exists(matrices):
            os.makedirs(matrices)
    '''
    # Step 1: Build Global-DSM model
    sentences = utils.MySentences(args.corpus_sent)  # a memory-friendly iterator
    gdsm = Word2Vec(sentences=sentences, size=args.size, window=args.window, sg=args.cbow, max_vocab_size=800000)
    gdsm.save(os.path.join(dsms_dir, 'gdsms.model'))
    
    # Step 2: Topic-based DSMs
    
    #Train LDA over corpus (document-level version). 
    #Topics are defined as distributions over a vocabulary.
    
    utils.lda_preparation(args.corpus_doc, lda_dir)
    ccorpus = corpora.MmCorpus(os.path.join(lda_dir, 'lda_bow'))
    dictionary = corpora.Dictionary.load(os.path.join(lda_dir, 'lda_dict'))
    lda = models.LdaMulticore(ccorpus, id2word=dictionary, num_topics=args.topics, workers=3, iterations=200)
    lda.save(os.path.join(lda_dir, str(args.topics)))

    
    #Split generic corpus (sentence level version) to topic-specific corpora using the trained LDA model.
    #We adopt a soft clustering scheme (threshold for clustering is set to 0.1).
    
    files = {}
    for topic in range(0, args.topics):
        files[topic] = open(os.path.join(crp_dir, str(topic)), 'w')
    utils.topic_corpora(lda, dictionary, files, args.corpus_sent)
    
    #Creation of Topic-based Distributional Semantic Models obtained via
    #running Word2Vec over the topic-based corpora.
        
    crp_dir = '../../../fenchri/topic_modeling/clusters/enwiki_docs.cln/0.1_post/50T/'
    tdsm = {}
    for topic in range(0, args.topics):
        sentences = utils.MySentences(os.path.join(crp_dir, 'topic_' + str(topic)))
        tdsm[topic] = Word2Vec(sentences=sentences, size=args.size, window=args.window, sg=args.cbow, max_vocab_size=100000, workers=8)
        tdsm[topic].save(os.path.join(tdsms_dir, str(topic)))

    '''
    # Step 3: Semantic mappings (using lists extracted from WordNet)
    
    #Map each topic-based vector space to the global semantic space (defined in Step 1).
    #The spaces are 'linked' using the representations of monosemous words that are the most 
    #prominent for each topic. (For monosemy definition we utilize WordNet)
    if args.supervised == 'supervised':
        gdsm = Word2Vec.load(os.path.join(dsms_dir, 'gdsms.model'))
        for topic in range(0, args.topics):
            logging.info('Semantic mapping for %d tdsm', topic)
            counter = i = 0
            tdsm_anchors, gdsm_anchors = [], []
            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
            while counter<args.anchors:
                if utils.monosemy(tdsm.wv.index2word[i]) and tdsm.wv.index2word[i] in gdsm.wv.vocab:
                    tdsm_anchors.append(utils.normalize_(tdsm[tdsm.wv.index2word[i]]))
                    gdsm_anchors.append(utils.normalize_(gdsm[tdsm.wv.index2word[i]]))
                    counter+=1
                i+=1
            print(counter)
            W = utils.procrustes(tdsm_anchors, gdsm_anchors)
            np.save(os.path.join(matrices, str(topic)), W)

    elif args.supervised == 'unsupervised':
       
        gdsm = Word2Vec.load(os.path.join(dsms_dir, 'gdsms.model'))
        for topic in range(0, args.topics):

            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
            max_sim_subset_threshold = min(len(tdsm.wv.vocab),2000)

            logging.info('Vocabulary size for topic %d: %d' %(topic, max_sim_subset_threshold))
            gdsm_emb, tdsm_emb, count, words = utils.load_norm(gdsm, tdsm, max_sim_subset_threshold)
            logging.info('Vocab size for intersection of tdsm-gdsm: %d' %(count))

            logging.info('Anchor searching for %d tdsm', topic)

            # Similarity matrices for source and target spaces
            gdsm_sims = (gdsm_emb).dot(gdsm_emb.T)
            tdsm_sims = (tdsm_emb).dot(tdsm_emb.T)

            for row_no, row in enumerate(gdsm_sims):
                for col_no, el in enumerate(row):
                    gdsm_sims[row_no, col_no] = (el-min(row))/(max(row)-min(row))
            for row_no, row in enumerate(tdsm_sims):
                for col_no, el in enumerate(row):
                    tdsm_sims[row_no, col_no] = (el-min(row))/(max(row)-min(row))

            sim_sim = utils.similarity_distances(gdsm_sims, tdsm_sims, count, method='cosine')

            # Anchor points selection
            anchors_ = sorted(range(len(sim_sim)), key=lambda i: sim_sim[i])[-args.anchors:]

            tdsm_anchors, gdsm_anchors = [], []
            for anc in anchors_:
                tdsm_anchors.append(tdsm_emb[anc])
                gdsm_anchors.append(gdsm_emb[anc])

            logging.info('Semantic mapping for %d tdsm', topic)
            W = utils.procrustes(tdsm_anchors, gdsm_anchors)
            np.save(os.path.join(matrices, str(topic)), W)

            del gdsm_sims, tdsm_sims, sim_sim, tdsm_anchors, gdsm_anchors, W, tdsm

    elif args.supervised == 'semi_supervised':
        gdsm = Word2Vec.load(os.path.join(dsms_dir, 'gdsms.model'))

        print("Loaded generic-based dsm")
        for topic in range(0, args.topics):

            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
            max_sim_subset_threshold = max(len(tdsm.wv.vocab), 20000)

            counter = i = 0
            master_list = []
            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
            print("Loaded topic-based dsm")

            while counter < args.initial_:
                if utils.monosemy(tdsm.wv.index2word[i]) and tdsm.wv.index2word[i] in gdsm.wv.vocab:
                    master_list += [tdsm.wv.index2word[i]]
                    counter += 1
                i += 1
            print("initialized master list")
            logging.info('Vocabulary size for topic %d: %d' % (topic, max_sim_subset_threshold))
            gdsm_emb_master , tdsm_emb_master = utils.load_norm_master(gdsm, tdsm, master_list)
            gdsm_emb, tdsm_emb, count , words= utils.load_norm(gdsm, tdsm, max_sim_subset_threshold)
            print("Finished loading normalized embeddings")

            logging.info('Vocab size for intersection of tdsm-gdsm: %d' % (count))

            logging.info('Anchor searching for %d tdsm', topic)

            # Similarity matrices for source and target spaces
            gdsm_sims = (gdsm_emb).dot(gdsm_emb_master.T)
            tdsm_sims = (tdsm_emb).dot(tdsm_emb_master.T)
            
            for row_no, row in enumerate(gdsm_sims):
                for col_no, el in enumerate(row):
                    gdsm_sims[row_no, col_no] = (el-min(row))/(max(row)-min(row))
            for row_no, row in enumerate(tdsm_sims):
                for col_no, el in enumerate(row):
                    tdsm_sims[row_no, col_no] = (el-min(row))/(max(row)-min(row))
            gdsm_sims = gdsm_sims*gdsm_sims
            tdsm_sims = tdsm_sims*tdsm_sims
            sim_sim = utils.similarity_distances(gdsm_sims, tdsm_sims, count, method='cosine')

            # Anchor points selection
            anchors_ = sorted(range(len(sim_sim)), key=lambda i: sim_sim[i])[-args.anchors:]
            #anchors_ = random.sample(range(count), 1000) # Random anchors

            tdsm_anchors, gdsm_anchors = [], []
            for anc in anchors_:
                tdsm_anchors.append(tdsm_emb[anc])
                gdsm_anchors.append(gdsm_emb[anc])

            logging.info('Semantic mapping for %d tdsm', topic)
            W = utils.procrustes(tdsm_anchors, gdsm_anchors)
            np.save(os.path.join(matrices, str(topic)), W)

            del gdsm_sims, tdsm_sims, sim_sim, tdsm_anchors, gdsm_anchors, W, tdsm
    else:
        gdsm = Word2Vec.load(os.path.join(dsms_dir, 'gdsms.model'))
        logging.info("Loaded generic-based dsm")

        word_freq_cross_topic = dict()
        for topic in range(0, args.topics):
            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))
            for wd in tdsm.wv.vocab.keys():
                if wd in word_freq_cross_topic:
                    word_freq_cross_topic[wd] +=1
                else:
                    word_freq_cross_topic[wd] = 1
        c = 0
        unified_topic_vocab = list()
        for k, v in word_freq_cross_topic.items():
            if v == 50:
                c += 1
                unified_topic_vocab += [k]
        total_sims = np.zeros((len(unified_topic_vocab),len(unified_topic_vocab)))

        for topic in tqdm(range(0, args.topics)):

            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))

            max_sim_subset_threshold = max(len(tdsm.wv.vocab), 20000)

            logging.info('Vocabulary size for topic %d: %d' % (topic, max_sim_subset_threshold))

            gdsm_emb, tdsm_emb, count, words = utils.load_norm(gdsm, tdsm, max_sim_subset_threshold,
                                                               extra_restriction=unified_topic_vocab)
            logging.info("Finished loading normalized embeddings")

            logging.info('Vocab size for intersection of tdsm-gdsm: %d' % (count))

            logging.info('Anchor searching for %d tdsm', topic)

            # Similarity matrices for source and target spaces
            tdsm_sims = (tdsm_emb).dot(tdsm_emb.T)

            tdsm_sims = preprocessing.minmax_scale(tdsm_sims.T).T
            total_sims += tdsm_sims
            del tdsm_sims, tdsm


        logging.info("Finished aggregating similarity matrices of the respective topics...")
        total_sims /= total_sims.shape[0]
        gdsm_sims = (gdsm_emb).dot(gdsm_emb.T)

        gdsm_sims = preprocessing.minmax_scale(gdsm_sims.T).T
        # Anchor points selection
        sim_sim = utils.similarity_distances(gdsm_sims, total_sims, count, method='cosine')
        anchors_ = sorted(range(len(sim_sim)), key=lambda i: sim_sim[i])[-args.anchors:]
        # anchors_ = random.sample(range(count), 1000) # Random anchors
        logging.info("Anchor points calculated...")

        for topic in tqdm(range(0, args.topics)):
            tdsm = Word2Vec.load(os.path.join(tdsms_dir, str(topic)))

            max_sim_subset_threshold = max(len(tdsm.wv.vocab), 20000)

            logging.info('Vocabulary size for topic %d: %d' % (topic, max_sim_subset_threshold))

            gdsm_emb, tdsm_emb, count, words = utils.load_norm(gdsm, tdsm, max_sim_subset_threshold,
                                                               extra_restriction=unified_topic_vocab)
            tdsm_anchors, gdsm_anchors = [], []
            for anc in anchors_:
                tdsm_anchors.append(tdsm_emb[anc])
                gdsm_anchors.append(gdsm_emb[anc])

            logging.info('Semantic mapping for %d tdsm', topic)
            W = utils.procrustes(tdsm_anchors, gdsm_anchors)
            np.save(os.path.join(matrices, str(topic)), W)


if __name__ == '__main__':
    main ()
