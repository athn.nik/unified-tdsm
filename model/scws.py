#!/usr/bin/python
"Apply UTDSM model to the contextual semantic similarity task."
import os
import logging
import argparse
import numpy as np
from gensim import corpora, models
from scipy.spatial.distance import cosine
from gensim.models import Word2Vec
from scipy.stats import spearmanr
from tqdm import tqdm
from utils import normalize_
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN
from sklearn.cluster import SpectralClustering
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import MiniBatchKMeans
from sklearn.mixture import GaussianMixture
import math

def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Unified model of multiple topic-based embeddings')
    # Both corpora are expected to be preprocessed
    parser.add_argument('--lda', help='path to trained lda model',
                        default='../../lda/enwiki_docs.cln_50T.lda')
    parser.add_argument('--dict', help='path to lda dictionary',
                        default='../../lda/enwiki_docs.cln.dict')
    parser.add_argument('--dataset', help='path to dataset',
                        default='../../SCWS.txt')
    parser.add_argument('--topics', help='the number of topics', type=int, default=50)
    parser.add_argument('--tdsms', help='path to tdsms', default='../../dsms/50/')
    parser.add_argument('--mappings', help='path to semantic mappings', default='../../matrices/unsupervised/')
    parser.add_argument('--results', help='path to semantic mappings', default='../../results/unsupervised/')
    parser.add_argument('--supervised', help='flag for self learning implementation', type=bool, default=False)
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    if not os.path.exists(args.results):
        os.makedirs(args.results)

    tm = models.LdaModel.load(args.lda, mmap='r')
    dict_ = corpora.Dictionary.load(args.dict)
    word_i, word_j, context_i, context_j, test_score = read_dataset(args.dataset)
    #topic_vectors_scores(args.tdsms, args.mappings, args.topics, word_i, word_j, context_i,
                            #context_j, tm, dict_, args.results)

    topic_vec1 = np.load(os.path.join(args.results, str('topic_vec1.npy')))
    topic_vec2 = np.load(os.path.join(args.results, str('topic_vec2.npy')))
    score1 = np.load(os.path.join(args.results, str('score1.npy')))
    score2 = np.load(os.path.join(args.results, str('score2.npy')))

    result1, result2 = avgsimc_smoothed(test_score, topic_vec1, score1, topic_vec2, score2)
    print(result1)

    #result = maxsimc(test_score, topic_vec1, score1, topic_vec2, score2)
    print(result2)


def read_dataset(dataset):
    '''
    Dataset parsing

    :param dataset: SCWS dataset
    :return: lists of words pairs, their contexts and ground truth similarty values
    '''

    word_i, word_j, context_i, context_j, ground_truth = [], [], [], [], []
    with open(dataset) as data:
        for i, line in enumerate(data):
            line = line.split("\t")
            word_i.append(line[0].strip())
            word_j.append(line[1].strip())
            ground_truth.append(line[2])
            context_i.append(line[3].lower().split())
            context_j.append(line[4].lower().split())
    return word_i, word_j, context_i, context_j, ground_truth

def topic_vectors_scores(tdsms, mappings, topics, word_i, word_j, context_i, context_j, tm, dict_, results):

    topic_vec1, score1, topic_vec2, score2, = [], [], [], []
    post_j = np.zeros((2003, int(topics)))
    post_i = np.zeros((2003, int(topics)))

    for index, (w_i, w_j, c_i, c_j) in enumerate(zip(word_i, word_j, context_i, context_j)):
        temp = sorted(tm.get_document_topics(dict_.doc2bow(c_i), minimum_probability=0), key=lambda tup: tup[0])
        temp = [float(x[1]) for x in temp]
        post_i[index] = temp
        temp = sorted(tm.get_document_topics(dict_.doc2bow(c_j), minimum_probability=0), key=lambda tup: tup[0])
        temp = [float(x[1]) for x in temp]
        post_j[index] = temp


    for topic in tqdm(range(topics)):
        tdsm = Word2Vec.load(os.path.join(tdsms, str(topic)))
        W = np.load(os.path.join(mappings, str(topic) + '.npy'))

        w_i_embeddings, w_j_embeddings = [], []
        junk_embedding = [0] * 300
        for index, (w_i, w_j, c_i, c_j) in enumerate(zip(word_i, word_j, context_i, context_j)):
            if w_i in tdsm.wv.vocab:
                w_i_embeddings.append(normalize_(tdsm[w_i]).dot(W))
            else:
                w_i_embeddings.append(junk_embedding)

            if w_j in tdsm.wv.vocab:
                w_j_embeddings.append(normalize_(tdsm[w_j]).dot(W))
            else:
                w_j_embeddings.append(junk_embedding)

        topic_vec1.append(w_i_embeddings)
        topic_vec2.append(w_j_embeddings)

    topic_vec1=np.swapaxes(topic_vec1,0,1)
    topic_vec2=np.swapaxes(topic_vec2,0,1)

    np.save(os.path.join(results, str('topic_vec1')), topic_vec1)
    np.save(os.path.join(results, str('topic_vec2')), topic_vec2)
    np.save(os.path.join(results, str('score1')), post_i)
    np.save(os.path.join(results, str('score2')), post_j)

    return topic_vec1, topic_vec2


def avgsimc(test_score, topic_vec1, score1, topic_vec2, score2):
    """
    Code for contextual similarity metric avgsimc
    calculation

    """
    avg_cos = []
    test_sub = []

    with tqdm(total=2003) as pbar:
        for word, vec in enumerate(topic_vec1):
            pbar.update(1)
            pair_cos = []
            probs = []
            p1 = (score1[word])
            p2 = (score2[word])

            if topic_vec1[word].any() and topic_vec2[word].any():
                for topic1, _ in enumerate(topic_vec1[word]):
                    for topic2, _ in enumerate(topic_vec2[word]):
                        if np.count_nonzero(topic_vec1[word][topic1]==0)<300 and np.count_nonzero(topic_vec2[word][topic2]==0)<300:
                            probs.append(p1[topic1] * p2[topic2])
                            pair_cos.append([(1 - cosine(topic_vec1[word][topic1], topic_vec2[word][topic2])) * p1[
                                topic1] * p2[topic2]])
                test_sub.append(test_score[word])
                avg_cos.append(np.sum(pair_cos) / np.sum(probs))
                # print(str(test_score[word]) + ' ' + str(np.mean(pair_cos)))

    rho, pval = spearmanr(test_sub, avg_cos)
    return rho

def clustering_(vectors, clust):
    #model = KMeans(n_clusters=clust)
    #model = SpectralClustering(n_clusters=clust)
    #model = MiniBatchKMeans(n_clusters=clust)
    #model = AgglomerativeClustering(n_clusters=clust, linkage="complete", affinity="cosine")
    model = GaussianMixture(n_components=clust, covariance_type='diag')
    model.fit(np.array(vectors))

    #labels = model.labels_.tolist()
    #labels = [i+1 for i in labels]
    return model.means_

def avgsimc_smoothed(test_score, topic_vec1, score1, topic_vec2, score2):
    """
    Code for contextual similarity metric avgsimc
    calculation

    """
    avg_cos = []
    max_cos = []
    test_sub = []

    with tqdm(total=2003) as pbar:
        for word, vec in enumerate(topic_vec1):
            pbar.update(1)
            pair_cos = []
            probs = []
            p1 = (score1[word])
            p2 = (score2[word])

            vectors1 = []
            vectors2 = []
            probs1 = []
            probs2 = []

            if topic_vec1[word].any() and topic_vec2[word].any():

                for topic1, _ in enumerate(topic_vec1[word]):
                    if np.count_nonzero(topic_vec1[word][topic1] == 0) < 300:
                        vectors1.append(topic_vec1[word][topic1])
                        probs1.append(p1[topic1])


                for topic2, _ in enumerate(topic_vec2[word]):
                    if np.count_nonzero(topic_vec2[word][topic2] == 0) < 300:
                        vectors2.append(topic_vec2[word][topic2])
                        probs2.append(p2[topic2])

                probs1 = [float(i) / sum(probs1) for i in probs1]
                probs2 = [float(i) / sum(probs2) for i in probs2]

                clust = math.ceil(0.7*len(probs1))

                if len(probs1)>clust:
                    labels1 = clustering_(vectors1, clust)
                else:
                    labels1 = list(range(len(probs1)))

                clust = math.ceil(0.7*len(probs2))

                if len(probs2)>clust:
                    labels2 = clustering_(vectors2, clust)
                else:
                    labels2 = list(range(len(probs2)))

                # Smoothed vectors and posterios for word_1
                smooth_post1 = np.zeros((max(labels1)+1))
                smooth_vectors1 = np.zeros((max(labels1)+1, 300))
                for i in range(max(labels1)+1):
                    cluster_occ = [j for j, x in enumerate(labels1) if x == i]
                    for x in cluster_occ:
                        smooth_vectors1[i] += vectors1[x]
                        smooth_post1[i] += probs1[x]
                    smooth_vectors1[i] /= len(cluster_occ)

                # Smoothed vectors and posterios for word_1
                smooth_post2 = np.zeros((max(labels2)+1))
                smooth_vectors2 = np.zeros((max(labels2)+1, 300))
                for i in range(max(labels2)+1):
                    cluster_occ = [j for j, x in enumerate(labels2) if x == i]
                    for x in cluster_occ:
                        smooth_vectors2[i] += vectors2[x]
                        smooth_post2[i] += probs2[x]
                    smooth_vectors2[i] /= len(cluster_occ)


                for i in range(max(labels1)+1):
                    for j in range(max(labels2)+1):
                        probs.append(smooth_post1[i] * smooth_post2[j])
                        pair_cos.append([(1 - cosine(smooth_vectors1[i], smooth_vectors2[j])) * smooth_post1[i]
                                     * smooth_post2[j]])

                max_topic1 = np.argmax(smooth_post1)
                max_topic2 = np.argmax(smooth_post2)
                max_pair_cos = (1 - cosine(smooth_vectors1[max_topic1], smooth_vectors2[max_topic2]))

                max_cos.append(max_pair_cos)

                test_sub.append(test_score[word])
                avg_cos.append(np.sum(pair_cos) / np.sum(probs))


    rho1, pval = spearmanr(test_sub, avg_cos)
    rho2, pval = spearmanr(test_sub, max_cos)

    return rho1, rho2


def maxsimc(test_score, topic_vec1, score1, topic_vec2, score2):
    """
    Code for contextual similarity metric maxsimc
    calculation

    """
    max_cos = []
    test_sub = []
    for word, vec in enumerate(topic_vec1):
        topic1 = np.argmax(score1[word])
        topic2 = np.argmax(score2[word])
        if topic_vec1[word][topic1].any() and topic_vec2[word][topic2].any():
            pair_cos = (1 - cosine(topic_vec1[word][topic1], topic_vec2[word][topic2]))
            max_cos.append(pair_cos)
            test_sub.append(test_score[word])
    rho, pval = spearmanr(np.array(test_sub), np.array(max_cos))
    return rho


if __name__ == '__main__':
    main()