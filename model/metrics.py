from scipy.stats import spearmanr
from scipy.spatial.distance import cosine
import numpy as np
from gensim.test.utils import datapath
from gensim.models import KeyedVectors

def load_embeddings_w2vec_format(vector_path, binary=True):   
    wv_from_text = KeyedVectors.load_word2vec_format(datapath(vector_path), binary=format)


def avgsimc(test_score, topic_vec1, score1, topic_vec2, score2):
    """
    Code for contextual similarity metric avgsimc
    calculation

    """
    avg_cos = []
    for word, vec in enumerate(topic_vec1):
        pair_cos = []
        p1 = (score1[word])
        p2 = (score2[word])
        for topic1,_ in enumerate(topic_vec1[word]):
            for topic2,_ in enumerate(topic_vec2[word]):
                pair_cos+=[(1-cosine(topic_vec1[word][topic1],topic_vec2[word][topic2]))*p1[topic1]*p2[topic2]]
        avg_cos+=[np.sum(pair_cos)]
    
    return spearmanr(test_score, avg_cos)[0]


def maxsimc(test_score, topic_vec1, score1, topic_vec2, score2):
    """
    Code for contextual similarity metric maxsimc
    calculation
    
    """

    all_pairs_cos = []
    for word, vec in enumerate(topic_vec1):
        topic1 = np.argmax(score1[word])
        topic2 = np.argmax(score2[word])
        pair_cos = (1-cosine(topic_vec1[word][topic1],topic_vec2[word][topic2])) 
        all_pairs_cos+=[pair_cos]
    return spearmanr(test_score, all_pairs_cos)[0]