import sklearn
import numpy as np
import data_loaders
import pickle
from tqdm import tqdm
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

def fit_classifier(clf, clf2,clf3,clf4,x_train, y_train, x_test, y_test):
    # split
    # learn the model
    clf_l =[clf,clf2,clf3,clf4]
    for classifier in clf_l:
        classifier.fit(x_train, y_train)

        # predict
        y_predicted = classifier.predict(x_test)
        # calculate percision
        print(classification_report(y_test, y_predicted))



def get_one_doc_topic_embeddings(words, dict_of_dicts_of_embeddings):
    t_e = np.zeros((len(words),50,300))
    for w_pos, w in enumerate(words):
        for t, e in dict_of_dicts_of_embeddings.items():
            if w in e:
                t_e[w_pos][t] = e[w]
    return t_e

def paraphrase_identification():
    clf = sklearn.svm.LinearSVC()
    clf2 = sklearn.svm.LinearSVC(C=0.7)
    clf3 = sklearn.svm.LinearSVC(C=0.3)
    clf4 = sklearn.svm.LinearSVC(C=0.9)

    data_rows_train, labels_train = data_loaders.fetch_paraphrase('train')
    data_rows_test, labels_test = data_loaders.fetch_paraphrase('test')
    topic_probs_train = data_loaders.fetch_lda(data_rows_train, task ='paraphrase')
    topic_probs_test = data_loaders.fetch_lda(data_rows_test, task ='paraphrase')

    fit_classifier(clf, clf2, clf3, clf4, topic_probs_train, labels_train, topic_probs_test, labels_test)

    with open('/home/nathan/Desktop/unified-tdsms/outputs_classification/all.pkl', 'rb') as f:
        topic_word_embedding_dict = pickle.load(f)

    doc_embed_train = np.zeros((len(data_rows_train), 300))
    for i, data_sample in enumerate(tqdm(data_rows_train)):
        vec_processed = get_one_doc_topic_embeddings(data_sample.split(), topic_word_embedding_dict)
        lda_weighted = np.zeros((vec_processed.shape[0], vec_processed.shape[2]))
        for word_id, wd in enumerate(vec_processed):
            lda_weighted[word_id] = ((wd.T * topic_probs_train[i]).T).sum(axis=0)

        doc_embed_train[i] = np.mean(lda_weighted, axis=0)

    doc_embed_test = np.zeros((len(data_rows_test), 300))
    for i, data_sample in enumerate(tqdm(data_rows_test)):
        vec_processed = get_one_doc_topic_embeddings(data_sample.split(), topic_word_embedding_dict)
        lda_weighted = np.zeros((vec_processed.shape[0], vec_processed.shape[2]))
        for word_id, wd in enumerate(vec_processed):
            lda_weighted[word_id] = ((wd.T * topic_probs_test[i]).T).sum(axis=0)
        doc_embed_test[i] = np.mean(lda_weighted, axis=0)

    mask_train = ~np.any(np.isnan(doc_embed_train), axis=1)
    doc_embed_train = doc_embed_train[mask_train]
    labels_train = labels_train[mask_train]
    mask_test = ~np.any(np.isnan(doc_embed_test), axis=1)
    doc_embed_test = doc_embed_test[mask_test]
    labels_test = labels_test[mask_test]

    fit_classifier(clf, clf2, clf3, clf4, doc_embed_train, labels_train, doc_embed_test, labels_test)


def classification_20news():
    clf = sklearn.svm.LinearSVC()
    clf2 = sklearn.svm.LinearSVC(C=0.7)
    clf3= sklearn.svm.LinearSVC(C=0.3)
    clf4 = sklearn.svm.LinearSVC(C=0.9)

    data_rows_train, labels_train, int2class = data_loaders.fetch_20n('train')
    data_rows_test, labels_test, _ = data_loaders.fetch_20n('test')
    topic_probs_train = data_loaders.fetch_lda(data_rows_train)
    topic_probs_test = data_loaders.fetch_lda(data_rows_test)

    fit_classifier(clf,clf2,clf3,clf4,topic_probs_train,labels_train,topic_probs_test,labels_test)


    with open('/home/nathan/Desktop/unified-tdsms/outputs_classification/all.pkl', 'rb') as f:
        topic_word_embedding_dict = pickle.load(f)


    doc_embed_train = np.zeros((len(data_rows_train),300))
    for i, data_sample in enumerate(tqdm(data_rows_train)):
        vec_processed = get_one_doc_topic_embeddings(data_sample.split(), topic_word_embedding_dict)
        lda_weighted = np.zeros((vec_processed.shape[0],vec_processed.shape[2]))
        for word_id, wd in enumerate(vec_processed):
            lda_weighted[word_id] = ((wd.T * topic_probs_train[i]).T).sum(axis=0)

        doc_embed_train[i] = np.mean(lda_weighted, axis=0)

    doc_embed_test = np.zeros((len(data_rows_test), 300))
    for i, data_sample in enumerate(tqdm(data_rows_test)):
        vec_processed = get_one_doc_topic_embeddings(data_sample.split(), topic_word_embedding_dict)
        lda_weighted = np.zeros((vec_processed.shape[0],vec_processed.shape[2]))
        for word_id, wd in enumerate(vec_processed):
            lda_weighted[word_id] = ((wd.T * topic_probs_test[i]).T).sum(axis=0)
        doc_embed_test[i] = np.mean(lda_weighted, axis=0)


    mask_train = ~np.any(np.isnan(doc_embed_train), axis=1)
    doc_embed_train = doc_embed_train[mask_train]
    labels_train = labels_train[mask_train]
    mask_test = ~np.any(np.isnan(doc_embed_test), axis=1)
    doc_embed_test = doc_embed_test[mask_test]
    labels_test = labels_test[mask_test]

    fit_classifier(clf, clf2,clf3,clf4, doc_embed_train, labels_train, doc_embed_test, labels_test)
    #fit_classifier(clf, topic_probs_train, labels_train, topic_probs_test, labels_test)
    fit_classifier(clf, clf2, clf3, clf4, topic_probs_train, labels_train, topic_probs_test, labels_test)

def main():
    #classification_20news()
    paraphrase_identification()

if __name__ == '__main__':
    main()
