from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from gensim.models import Word2Vec
from gensim import corpora, models
import os
import re
import numpy as np
import string
import pickle
from nltk.corpus import stopwords
import nltk
from stop_words import get_stop_words
from tqdm import tqdm
from utils import normalize_
from itertools import islice
import sys

nltk.download('stopwords')
with open('../stopwords.txt', 'r') as fl:
    stop_words_custom = fl.readlines()
    stop_words_custom = [x.strip() for x in stop_words_custom]

stopwords_list = set(list(get_stop_words('en')) + list(stopwords.words('english')) + stop_words_custom)


def chunks(data, SIZE=10000):
    it = iter(data)
    for i in range(0, len(data), SIZE):
        yield {k:data[k] for k in islice(it, SIZE)}


def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    string = string.strip().lower()
    string_proc = []
    for _, wd in enumerate(string.split()):
        if wd not in stopwords_list:
            string_proc += [wd]

    string = ' '.join(string_proc)

    return string

def fetch_20n(subset : str):
    news = fetch_20newsgroups(subset=subset,remove=('headers', 'footers', 'quotes'))
    data_rows = news.data
    for i,row in enumerate(data_rows):
        data_rows[i] = clean_str(row).translate(str.maketrans('','',string.punctuation))

    classes = news.target
    int2class = news.target_names
    return data_rows,classes,int2class

def fetch_paraphrase(subset):

    with open('../paraphrase_dataset/msr_paraphrase_' + subset + '.txt','r') as f:
        raw_data = f.readlines()[1:]
    data_rows = [[] for i in range(len(raw_data))]
    data_labels = []
    for i, r in enumerate(raw_data):
        data_rows[i] = [r.split('\t')[3]]
        data_rows[i] += [r.split('\t')[4]]
        lbl = int(r.split('\t')[0])
        data_labels += [lbl]

    for i,row in enumerate(data_rows):
        for j, s in enumerate(row):
            data_rows[i][j] = clean_str(s).translate(str.maketrans('','',string.punctuation))
    return data_rows, data_labels

def fetch_lda(data_rows, task='tex_classification' ):
    tm = models.LdaModel.load('../../lda/enwiki_docs.cln_50T.lda', mmap='r')
    dict_ = corpora.Dictionary.load('../../lda/enwiki_docs.cln.dict')
    gdsm = Word2Vec.load(os.path.join(os.getcwd(), '../../dsms/gdsms.model'))
    # construct dict with words: embeddings
    if task == 'paraphrase':
        topic_probs = np.zeros((len(data_rows),2, 50))
    else:
        topic_probs = np.zeros((len(data_rows),50))
    for index, row in enumerate(data_rows):
        if task=='paraphrase':
            topic_probs[index][0] = [float(x[1]) for x in sorted(
                                                        tm.get_document_topics(
                                                                               dict_.doc2bow(row[0].split()),
                                                                                   minimum_probability=0),
                                                                              key=lambda tup: tup[0]) ]

            topic_probs[index][1] = [float(x[1]) for x in sorted(
                                                        tm.get_document_topics(
                                                                               dict_.doc2bow(row[1].split()),
                                                                                   minimum_probability=0),
                                                                              key=lambda tup: tup[0]) ]
        else:
            topic_probs[index] = [float(x[1]) for x in sorted(
                                                        tm.get_document_topics(
                                                                               dict_.doc2bow(row.split()),
                                                                               minimum_probability=0),
                                                                               key=lambda tup: tup[0]) ]
    return topic_probs



#Code for embeddings prepation

# prepare dict with word embeddings

#embeddings_dict = dict()
def gather_n_save_topic_embeddings(path_to_dsm, path_to_mats, save_path):
    embe = dict()
    id = 0
    for topic in tqdm(range(50)):
        tdsm = Word2Vec.load(os.path.join('../../dsms/50/', str(topic)))
        W = np.load(os.path.join('../../matrices/unsupervised/50/', str(topic) + '.npy'))
        embeddings_dict = dict()
        for word in tdsm.wv.vocab:
            embeddings_dict[word] = normalize_(tdsm[word]).dot(W)
        embe[id]= embeddings_dict
        id+=1
        with open('/home/nathan/Desktop/unified-tdsms/outputs_classification/'+ str(topic) + '.pkl', 'wb') as f:
            pickle.dump(embeddings_dict, f)
    with open('/home/nathan/Desktop/unified-tdsms/outputs_classification/all.pkl', 'wb') as f:
        pickle.dump(embe, f)






'''

# with open('/home/nathan/Desktop/unified-tdsms/outputs_classification/sample.pkl','rb') as f:
#     x = pickle.load(f)



#
# for k,v in x:


# print(embeddings_dict['rock'])
# count_vect = CountVectorizer()
# X_train_counts = count_vect.fit
# _transform(data_rows)

#for word, tpc_embed in embeddings_dict:

# for i in range(len(embeddings_dict),):
"""

from itertools import islice

def chunks(data, SIZE=10000):
    it = iter(data)
    for i in xrange(0, len(data), SIZE):
        yield {k:data[k] for k in islice(it, SIZE)}
d4 = dict(d1)
d4.update(d2)
d4.update(d3)

'''