# Unified-TDSM

Code for paper "Unified model of multiple topic-based distributional semantic representations"

It is proposed to use python virtual virtual environments to handle different projects. To create and activate a virtual environment for python 3 you need to run:
```
$ python3.6 -m venv venv
$ source venv/bin/activate
```
Make sure you have updated your setuptools and pip versions using:
```
$ pip install --upgrade setuptools pip
```
# How to set up
```python3.6 -m venv your_venv_name
   source your_env_name/bin/activate
```
Your env name *should* start with "env".
Then:
`pip install -r requirements.txt`

# Code Structure
utils: supplementary functions<br />
utdsms: Two step procedure
* [Topic-based Distributional Semantic Models](https://ieeexplore.ieee.org/document/8334459/)
* Semantic Mappings

